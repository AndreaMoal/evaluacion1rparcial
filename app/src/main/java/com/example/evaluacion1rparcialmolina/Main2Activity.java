package com.example.evaluacion1rparcialmolina;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {

    TextView texto;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        texto =(TextView)findViewById(R.id.rectxt);
        Bundle bundle = this.getIntent().getExtras();

        double resultt;
        double toneladas;
        toneladas = 0.0004536;
        double libras;
        libras = Integer.parseInt(bundle.getString("dato"));
        resultt=Double.valueOf((libras*toneladas));



        texto.setText(resultt + ""+"toneladas");

        texto.setText(String.format("%.4f", resultt));

        Log.e("operacion","onCreate");
    }

}

