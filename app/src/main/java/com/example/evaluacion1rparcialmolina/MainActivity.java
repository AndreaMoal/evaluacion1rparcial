package com.example.evaluacion1rparcialmolina;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    Button buttonConver;
    EditText cajaDatos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonConver = (Button) findViewById(R.id.button);
        cajaDatos = (EditText)findViewById(R.id.parametrosText);
        buttonConver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                Bundle bundle = new Bundle();
                bundle.putString("dato",cajaDatos.getText().toString());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });


    }
}
